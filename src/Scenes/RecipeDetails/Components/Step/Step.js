import React from 'react';
import PropTypes from 'prop-types';

import Enumerator from '../../../../Components/Enumerator/Enumerator';

import './Step.scss';

const Step = ({ number, description }) => (
  <div className="step">
    <Enumerator number={number} />
    <div className="step__description">{description}</div>
  </div>
);

Step.propTypes = {
  number: PropTypes.number.isRequired,
  description: PropTypes.string.isRequired,
};

export default Step;
