import {
  RECIPE_FIND_REQUEST, RECIPE_FIND_SUCCESS, RECIPE_FIND_FAILURE,
} from '../constants/recipeDetails';

export const initialState = {
  isFetching: false,
  didInvalidate: false,
  value: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case RECIPE_FIND_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case RECIPE_FIND_SUCCESS:
      return {
        ...state,
        value: action.recipeDetails,
        isFetching: false,
      };
    case RECIPE_FIND_FAILURE:
      return {
        ...state,
        error: action.err.message,
        isFetching: false,
        didInvalidate: true,
      };
    default:
      return state;
  }
};
