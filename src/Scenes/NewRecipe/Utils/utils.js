import Joi from 'joi';

export function validateFields(schema, fields, errorMessage) {
  const { error } = Joi.validate(fields, schema, { abortEarly: false });
  if (error) {
    return error.details.reduce((res, { context, message }) => {
      if (!context.key) return errorMessage || message;

      res[context.key] = errorMessage || message;
      return res;
    }, {});
  }
  return false;
}

export function validateRecipe(schema, fields) {
  const errors = Object.keys(schema).reduce((res, key) => {
    const { error } = Joi.validate(fields[key], schema[key]);
    if (error) {
      const { message } = error.details[0];
      res[key] = message.replace(/(".*")/g, `"${key}"`);
    }
    return res;
  }, {});

  return (Object.keys(errors).length && errors) || false;
}
