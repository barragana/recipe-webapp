import React from 'react';
import { storiesOf } from '@storybook/react';

import Ingredients from './Ingredients';
import Seasonings from './Seasonings';
import Tick from './Tick';
import Edit from './Edit';
import Remove from './Remove';
import Add from './Add';
import Done from './Done';
import CleanIcon from './Clean';
import SearchIcon from './Search';

storiesOf('Components', module)
  .add('IngredientsIcon', () => (
    <Ingredients width={40} />
  ))
  .add('SeasoningsIcon', () => (
    <Seasonings width={40} />
  ))
  .add('TickIcon', () => (
    <Tick width={20} />
  ))
  .add('EditIcon', () => (
    <Edit width={40} />
  ))
  .add('RemoveIcon', () => (
    <Remove width={40} />
  ))
  .add('AddIcon', () => (
    <Add width={40} />
  ))
  .add('DoneIcon', () => (
    <Done width={40} />
  ))
  .add('CleanIcon', () => (
    <CleanIcon />
  ))
  .add('SearchIcon', () => (
    <SearchIcon />
  ));
