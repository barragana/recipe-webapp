import React, { Component } from 'react';
import PropTypes from 'prop-types';

import SearchIcon from '../SVGs/Search';
import CleanIcon from '../SVGs/Clean';

import './Search.scss';

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
    };

    this.handleClearClick = this.handleClearClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e) {
    clearTimeout(this.timeoutID);
    this.timeoutID = setTimeout(this.props.onChange, 500, e.target.value);
    this.setState({ value: e.target.value });
  }

  handleClearClick() {
    this.el.focus();
    this.setState({ value: '' });
    this.props.onChange('');
  }

  renderIcon() {
    if (this.state.value) {
      return (
        <span
          className="search__clear"
          onClick={this.handleClearClick}
          role="button"
          tabIndex="0"
        >
          <CleanIcon />
        </span>
      );
    }

    return <SearchIcon />;
  }

  render() {
    return (
      <div className="search">
        <input
          className="search__input"
          ref={(el) => { this.el = el; }}
          onChange={this.handleChange}
          placeholder={this.props.placeholder}
          value={this.state.value}
        />
        { this.renderIcon() }
      </div>
    );
  }
}

Search.propTypes = {
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string.isRequired,
  value: PropTypes.string,
};

Search.defaultProps = {
  value: '',
};

export default Search;
