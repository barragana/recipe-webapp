import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Actions from './Actions';

storiesOf('NewRecipe/Components', module)
  .add('Actions: default', () => (
    <Actions onClick={action('click')} />
  ))
  .add('Actions: editing', () => (
    <Actions status="editing" onClick={action('click')} />
  ))
  .add('Actions: done', () => (
    <Actions status="done" onClick={action('click')} />
  ));
