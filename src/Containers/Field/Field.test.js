import React from 'react';
import { shallow, mount } from 'enzyme';

import Field from './Field';
import FormGroup from '../../Components/FormGroup/FormGroup';
import Label from '../../Components/Label/Label';
import Input from '../../Components/Input/Input';
import Textarea from '../../Components/Textarea/Textarea';

describe('Containers', () => {
  describe('Field', () => {
    describe('should', () => {
      const mockOnChange = jest.fn();
      const wrapper = shallow(
        <Field
          id="input_name"
          type="input"
          onChange={mockOnChange}
          placeholder="placeholder text"
        />,
      );

      it(
        'render',
        () => {
          expect(wrapper.html()).not.toBeNull();
        },
      );
      it(
        'render FormGroup',
        () => {
          expect(wrapper.find(FormGroup).exists()).toEqual(true);
        },
      );
      it(
        'render Input',
        () => {
          expect(wrapper.find(Input).exists()).toEqual(true);
        },
      );
      it(
        'render no Label when no children',
        () => {
          expect(wrapper.find(Label).exists()).toEqual(false);
        },
      );
    });

    describe('should', () => {
      const mockOnChange = jest.fn();
      const wrapper = shallow(
        <Field
          id="input_name"
          type="textarea"
          onChange={mockOnChange}
          placeholder="placeholder text"
        >
          Label
        </Field>,
      );

      it(
        'render Textare',
        () => {
          expect(wrapper.find(Textarea).exists()).toEqual(true);
        },
      );
      it(
        'render Label when has children',
        () => {
          expect(wrapper.find(Label).exists()).toEqual(true);
        },
      );
    });

    describe('should', () => {
      const mockOnChange = jest.fn();
      const event = { target: { id: 'name', value: 'deu' } };

      const wrapperTyping = mount(
        <Field
          id="input_name"
          onChange={mockOnChange}
          placeholder="placeholder text"
        />,
      );

      let input = wrapperTyping.find('input');
      input.simulate('change', event);
      input = wrapperTyping.update().find('input');

      it(
        'render typed text into input',
        () => {
          expect(input.props().value).toEqual('deu');
        },
      );

      it(
        'trigger mockOnChange when typing on input',
        () => {
          expect(mockOnChange).toBeCalled();
          expect(mockOnChange).toBeCalledWith({
            id: 'name',
            value: 'deu',
          });
        },
      );
    });
  });
});
