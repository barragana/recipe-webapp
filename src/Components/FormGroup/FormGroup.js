import React from 'react';
import PropTypes from 'prop-types';

import './FormGroup.scss';

const Form = ({ children }) => (
  <form className="form-group">
    {children}
  </form>
);

Form.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Form;
