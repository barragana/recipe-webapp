const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = require('./config');

module.exports = merge(config, {
  // the main entry point for our application's frontend JS
  entry: [
    path.join(__dirname, '../src/index.js'),
  ],

  output: {
    // the filename of the compiled bundle, e.g. /assets/bundle.js
    filename: '[name]-[chunkhash].js',
  },

  target: 'web',

  mode: 'production',

  devtool: 'source-map',

  stats: 'errors-only',

  plugins: [
    new ExtractTextPlugin('[name]-[chunkhash].css'),
    new UglifyJsPlugin({
      cache: true,
      sourceMap: true,
    }),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('production'),
    }),
    new HtmlWebpackPlugin({
      files: {
        css: ['[name]-[chunkhash].css'],
        js: ['[name]-[chunkhash].js'],
      },
      hash: true,
      inject: false,
      meta: { viewport: 'width=device-width,minimum-scale=1,initial-scale=1' },
      template: '../src/index.html',
      title: 'Recipe Amazing World',
    }),
  ],
});
