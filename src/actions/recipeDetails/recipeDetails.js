import axios from 'axios';

import store from '../../store';

import {
  RECIPE_FIND_REQUEST, RECIPE_FIND_SUCCESS, RECIPE_FIND_FAILURE,
} from '../../constants/recipeDetails';

import { recipesListSuccess } from '../recipesList/recipesList';

function recipeFindRequest() {
  return {
    type: RECIPE_FIND_REQUEST,
  };
}

export function recipeFindSuccess(recipeDetails) {
  return {
    type: RECIPE_FIND_SUCCESS,
    recipeDetails,
  };
}

export function recipeFindFailure(err) {
  return {
    type: RECIPE_FIND_FAILURE,
    err,
  };
}

export function processRecipeDetails(dispatch, list, id) {
  const recipeDetails = findRecipe(list, id);
  if (recipeDetails) {
    return dispatch(recipeFindSuccess(recipeDetails));
  }

  dispatch(recipeFindFailure({ message: 'recipe not found' }));
}

function findRecipe(list, id) {
  const parsedId = parseInt(id, 10);
  return list.find(recipe => recipe.id === parsedId);
}

export default id => (
  (dispatch) => {
    dispatch(recipeFindRequest());
    const { recipesList } = store.getState();

    if (isNaN(id)) return recipeFindFailure({ message: 'id is not a number' });

    if (recipesList.value.length) {
      return processRecipeDetails(dispatch, recipesList.value, id);
    }

    axios.get(
      'https://private-cc19d4-reactnativemockapi.apiary-mock.com/recipes',
    )
      .then(
        ({ data }) => {
          dispatch(recipesListSuccess(data));
          processRecipeDetails(dispatch, data, id);
        },
        (e) => {
          dispatch(recipeFindFailure(e));
        },
      );
  }
);
