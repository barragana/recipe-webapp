import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import requestRecipesList, { filterRecipesList }
  from '../../actions/recipesList/recipesList';

import RecipesList from './Components/FiguresList/FiguresList';
import Spinner from '../../Components/Spinner/Spinner';
import Search from '../../Components/Search/Search';

import './Home.scss';

class Home extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  componentDidMount() {
    this.props.dispatchRequestRecipesList();
  }

  handleClick(id) {
    this.props.history.push(`/recipes/${id}`);
  }

  render() {
    const {
      recipesList: { isFetching, value, search, didInvalidate, error },
      dispatchFilterRecipesList,
    } = this.props;

    if (didInvalidate) {
      return (
        <div className="home--center">
          <h2>{error}</h2>
        </div>
      );
    }

    if (isFetching || !value.length) {
      return (
        <div className="home--center">
          <Spinner size="large" />
        </div>
      );
    }

    const list = (search.isSearching && search.value) || value;
    return (
      <div className="home">
        <div className="home__search">
          <Search
            list={value}
            onChange={dispatchFilterRecipesList}
            placeholder="Search for ingredient"
          />
        </div>
        <div className="home__recipes-list">
          <RecipesList figuresList={list} onClick={this.handleClick} />
        </div>
      </div>
    );
  }
}

Home.propTypes = {
  dispatchRequestRecipesList: PropTypes.func.isRequired,
  dispatchFilterRecipesList: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired,
  recipesList: PropTypes.shape({
    didInvalidate: PropTypes.bool,
    isFetching: PropTypes.bool,
    value: PropTypes.array,
  }).isRequired,
};

const mapStateToProps = state => ({
  recipesList: { ...state.recipesList },
});

const mapDispatchToProps = dispatch => bindActionCreators({
  dispatchFilterRecipesList: filterRecipesList,
  dispatchRequestRecipesList: requestRecipesList,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Home);
