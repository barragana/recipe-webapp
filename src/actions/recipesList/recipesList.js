import axios from 'axios';

import {
  RECIPES_LIST_REQUEST, RECIPES_LIST_SUCCESS, RECIPES_LIST_FAILURE,
  FILTER_RECIPE_LIST,
} from '../../constants/recipesList';

function recipesListRequest() {
  return {
    type: RECIPES_LIST_REQUEST,
  };
}

export function recipesListSuccess(recipesList) {
  return {
    type: RECIPES_LIST_SUCCESS,
    recipesList,
  };
}

export function recipesListFailure(err) {
  return {
    type: RECIPES_LIST_FAILURE,
    err,
  };
}

export function filterRecipesList(term) {
  return {
    type: FILTER_RECIPE_LIST,
    term,
  };
}

export default () => (
  (dispatch) => {
    dispatch(recipesListRequest());
    axios.get(
      'https://private-cc19d4-reactnativemockapi.apiary-mock.com/recipes',
    )
      .then(
        ({ data }) => { dispatch(recipesListSuccess(data)); },
        (e) => {
          dispatch(recipesListFailure(e));
        },
      );
  }
);
