import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Input from './Input';

storiesOf('Components', module)
  .add('Input: default', () => (
    <Input id="input_name" onChange={action('input-change')} />
  ))
  .add('Input: disabled', () => (
    <Input id="input_name" onChange={action('input-change')} disabled />
  ))
  .add('Input: error', () => (
    <Input id="input_name" onChange={action('input-change')} error />
  ));
