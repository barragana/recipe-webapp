import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import Field from '../../Containers/Field/Field';
import Button from '../../Components/Button/Button';
import Spinner from '../../Components/Spinner/Spinner';
import FieldError from '../../Components/FieldError/FieldError';
import RawNewIngredients from './Components/NewIngredients/NewIngredients';
import RawNewItems from './Components/NewItems/NewItems';

import withDynamicFieldAddition from './HOC/withDynamicFieldAddition';

import saveNewRecipe, { singleFieldChange }
  from '../../actions/newRecipe/newRecipe';

import './NewRecipe.scss';


const NewIngredients = withDynamicFieldAddition(
  RawNewIngredients, 'ingredients',
);
const NewSeasonings = withDynamicFieldAddition(RawNewItems, 'seasonings');
const NewSteps = withDynamicFieldAddition(RawNewItems, 'steps');

const NewRecipe = ({
  dispatchSingleFieldChange, dispatchSaveNewRecipe, newRecipe,
}) => {
  const {
    value: { name, description, image }, isSaving, errors, didInvalidate,
  } = newRecipe;

  return (
    <Fragment>
      {
        isSaving && (
          <div className="new-recipe__spinner">
            <Spinner size="large" />
          </div>
        )
      }
      <main className="new-recipe">
        <h2 className="new-recipe__header">
          Add New Recipe
        </h2>
        <div className="new-recipe__fields">
          <Field id="name" onChange={dispatchSingleFieldChange} placeholder="Recipe Name" value={name} error={errors.name}>
            Recipe Name
          </Field>
          <Field id="description" type="textarea" onChange={dispatchSingleFieldChange} placeholder="Recipe Description" value={description} error={errors.description}>
            Recipe Description
          </Field>
          <Field id="image" onChange={dispatchSingleFieldChange} placeholder="Image url" value={image} error={errors.image}>
            Image url
          </Field>
          <div className="new-recipe__ingredients">
            <label className="new-recipe__label">Ingredients</label>
            <NewIngredients />
            {
              didInvalidate && errors.ingredients &&
              <FieldError>{errors.ingredients}</FieldError>
            }
          </div>
          <div className="new-recipe__seasonings">
            <label className="new-recipe__label">Seasonings</label>
            <NewSeasonings />
            {
              didInvalidate && errors.seasonings &&
              <FieldError>{errors.seasonings}</FieldError>
            }
          </div>
          <div className="new-recipe__steps">
            <label className="new-recipe__label">Instructions</label>
            <NewSteps />
            {
              didInvalidate && errors.steps &&
              <FieldError>{errors.steps}</FieldError>
            }
          </div>
        </div>
        <div className="new-recipe__submit">
          <Button onClick={dispatchSaveNewRecipe}>Add New Recipe</Button>
        </div>
      </main>
    </Fragment>
  );
};

NewRecipe.propTypes = {
  dispatchSingleFieldChange: PropTypes.func.isRequired,
  dispatchSaveNewRecipe: PropTypes.func.isRequired,
  newRecipe: PropTypes.shape({
    didInvalidate: PropTypes.bool,
    isSaving: PropTypes.bool,
    value: PropTypes.object,
    new: PropTypes.object,
  }).isRequired,
};

const mapStateToProps = state => ({
  newRecipe: state.newRecipe,
});

const mapDispatchToProps = dispatch => bindActionCreators({
  dispatchSingleFieldChange: singleFieldChange,
  dispatchSaveNewRecipe: saveNewRecipe,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(NewRecipe);
