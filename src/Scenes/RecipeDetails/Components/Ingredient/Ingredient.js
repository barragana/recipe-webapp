import React from 'react';
import PropTypes from 'prop-types';

import Tick from '../../../../Components/SVGs/Tick';

import './Ingredient.scss';

const Ingredient = ({ amount, name, unit }) => (
  <div className="ingredient">
    <Tick width={20} />
    <div className="ingredient__content">
      <span className="ingredient__content__amount">{amount} {unit}</span>
      <span className="ingredient__content__name">{name}</span>
    </div>
  </div>
);

Ingredient.propTypes = {
  amount: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  name: PropTypes.string.isRequired,
  unit: PropTypes.string,
};

Ingredient.defaultProps = {
  unit: 'unit',
};

export default Ingredient;
