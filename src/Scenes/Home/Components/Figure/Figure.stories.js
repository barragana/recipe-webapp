import React from 'react';
import { storiesOf } from '@storybook/react';

import Figure from './Figure';

storiesOf('Home/Components', module)
  .add('Figure', () => (
    <Figure
      src="https://nutrition-assets.freeletics.com/recipes/large/mustard_deviled_eggs.jpg"
      caption="Mustard Deviled Eggs"
    />
  ));
