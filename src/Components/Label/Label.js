import React from 'react';
import PropTypes from 'prop-types';

import './Label.scss';

const Label = ({ htmlFor, ...props }) => (
  <label htmlFor={htmlFor} className="label" {...props} />
);

Label.propTypes = {
  htmlFor: PropTypes.string.isRequired,
};

export default Label;
