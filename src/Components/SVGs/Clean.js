import React from 'react';

const Clean = () => (
  <svg viewBox="0 0 9 9" width="9" height="9" xmlns="http://www.w3.org/2000/svg">
    <line x1="0" y1="0" x2="9" y2="9" strokeWidth="1" stroke="#807877" />
    <line x1="9" y1="0" x2="0" y2="9" strokeWidth="1" stroke="#807877" />
  </svg>
);

export default Clean;
