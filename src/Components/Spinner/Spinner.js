import React from 'react';
import classnames from 'classnames';
import PropTypes from 'prop-types';

import './Spinner.scss';

const Spinner = ({ size }) => (
  <div className="spinner__container">
    <div className={classnames('spinner', size && `spinner__${size}`)} />
  </div>
);

Spinner.propTypes = {
  size: PropTypes.string,
};

Spinner.defaultProps = {
  size: '',
};

export default Spinner;
