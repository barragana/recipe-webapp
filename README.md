# recipe-webapp

Basic application to list recipes and create new ones.

## Coding - Quick Overview

It is strongly suggested to use yarn, since I did not test app with npm.

```sh
yarn            # to install dependencies
yarn lint       # to test code style with eslint
yarn storybook  # to start recipe-webapp components library locally at localhost:9001
yarn dev        # to start server at localhost:3030
```

## Known small issues
* missing screenshot tests with jest/storybook

### Linting

Linting using airbnb standards

### Testing

Make use of enzyme and jest to better test components, actions and reducers.
Not all of them are cover because some there is not much logic to be tested.
