import React, { Component } from 'react';
import PropTypes from 'prop-types';

import FormGroup from '../../Components/FormGroup/FormGroup';
import Label from '../../Components/Label/Label';
import Input from '../../Components/Input/Input';
import Textarea from '../../Components/Textarea/Textarea';
import FieldError from '../../Components/FieldError/FieldError';

import './Field.scss';

const FieldComponents = {
  input: Input,
  textarea: Textarea,
};

class Field extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: props.value,
    };

    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps({ value }) {
    this.setState({ value });
  }

  handleChange(e) {
    e.stopPropagation();

    this.setState({ value: e.target.value });
    this.props.onChange({ id: e.target.id, value: e.target.value });
  }

  render() {
    const { children, id, type, error, ...props } = this.props;
    const FieldComponent = FieldComponents[type] || Input;

    return (
      <div className="field">
        <FormGroup error={!!error}>
          { children && <Label htmlFor={id}>{children}</Label>}
          <FieldComponent
            {...props}
            error={!!error}
            id={id}
            onChange={this.handleChange}
            value={this.state.value}
          />
          {error && <FieldError>{error}</FieldError>}
        </FormGroup>
      </div>
    );
  }
}

Field.propTypes = {
  children: PropTypes.node,
  error: PropTypes.string,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onChange: PropTypes.func.isRequired,
  type: PropTypes.string,
  value: PropTypes.string,
};

Field.defaultProps = {
  children: null,
  error: '',
  type: 'input',
  value: '',
};

export default Field;
