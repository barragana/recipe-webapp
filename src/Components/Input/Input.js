import React from 'react';
import PropTypes from 'prop-types';

import './Input.scss';

const Input = ({ disabled, error, id, onChange, placeholder, value }) => (
  <input
    className={`input${(error && ' input--error') || ''}`}
    id={id}
    onChange={onChange}
    placeholder={placeholder}
    value={value}
    disabled={disabled}
  />
);

Input.propTypes = {
  disabled: PropTypes.bool,
  error: PropTypes.bool,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string,
};

Input.defaultProps = {
  disabled: false,
  error: false,
  placeholder: '',
  value: '',
};

export default Input;
