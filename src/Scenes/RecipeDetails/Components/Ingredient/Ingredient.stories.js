import React from 'react';
import { storiesOf } from '@storybook/react';

import Ingredient from './Ingredient';

const mockIngredient = {
  amount: '2',
  name: 'Dijon mustard',
  unit: 'tbsp',
};

storiesOf('RecipeDetails', module)
  .add('Ingredient', () => (
    <div style={{ width: '100%', background: '#f8f8f8', height: '100%', overflow: 'auto' }}>
      <Ingredient
        amount={mockIngredient.amount}
        name={mockIngredient.name}
        unit={mockIngredient.unit}
      />
    </div>
  ));
