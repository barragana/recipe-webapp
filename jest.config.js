module.exports = {
  roots: [
    'src/',
  ],
  moduleDirectories: [
    'node_modules',
    'src/',
  ],
  setupTestFrameworkScriptFile: '<rootDir>/config/setupTests.js',
  moduleNameMapper: {
    '\\.(scss|css)$': 'identity-obj-proxy',
  },
  verbose: true,
};
