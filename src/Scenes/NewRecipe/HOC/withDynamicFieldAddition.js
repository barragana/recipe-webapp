import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import {
  groupFieldChange, clickActionToGroupField,
} from '../../../actions/newRecipe/newRecipe';
import {
  ADD_GROUP_FIELD, EDIT_GROUP_FIELD, REMOVE_GROUP_FIELD, DONE_GROUP_FIELD,
} from '../../../constants/newRecipe';

const actionMap = {
  add: ADD_GROUP_FIELD,
  edit: EDIT_GROUP_FIELD,
  remove: REMOVE_GROUP_FIELD,
  done: DONE_GROUP_FIELD,
};

function withDynamicFieldAddition(WrappedComponent, groupId) {
  class NewField extends Component {
    constructor(props) {
      super(props);

      this.handleActionClick = this.handleActionClick.bind(this);
      this.handleFieldChange = this.handleFieldChange.bind(this);
    }

    handleFieldChange(groupItem) {
      this.props.dispatchGroupFieldChange(groupId, groupItem);
    }

    handleActionClick({ id, action }) {
      this.props.dispatchClickActionToGroupField(
        actionMap[action],
        { groupId, id },
      );
    }

    render() {
      const { newRecipe } = this.props;
      return (
        <WrappedComponent
          items={newRecipe.value[groupId]}
          newItem={newRecipe.new[groupId]}
          onActionClick={this.handleActionClick}
          onFieldChange={this.handleFieldChange}
        />
      );
    }
  }

  NewField.propTypes = {
    dispatchGroupFieldChange: PropTypes.func.isRequired,
    dispatchClickActionToGroupField: PropTypes.func.isRequired,
    newRecipe: PropTypes.shape({
      didInvalidate: PropTypes.bool,
      isFetching: PropTypes.bool,
      value: PropTypes.object,
      new: PropTypes.object,
    }).isRequired,
  };

  const mapStateToProps = state => ({
    newRecipe: state.newRecipe,
  });

  const mapDispatchToProps = dispatch => bindActionCreators({
    dispatchGroupFieldChange: groupFieldChange,
    dispatchClickActionToGroupField: clickActionToGroupField,
  }, dispatch);

  return connect(
    mapStateToProps,
    mapDispatchToProps,
  )(NewField);
}

export default withDynamicFieldAddition;
