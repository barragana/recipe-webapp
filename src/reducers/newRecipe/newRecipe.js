import cloneDeep from 'lodash.clonedeep';
import Joi from 'joi';

import {
  SINGLE_FIELD_CHANGE, GROUP_FIELD_CHANGE, ADD_GROUP_FIELD, EDIT_GROUP_FIELD,
  REMOVE_GROUP_FIELD, DONE_GROUP_FIELD, NEW_RECIPE_SAVE_REQUEST,
  NEW_RECIPE_SAVE_SUCCESS, NEW_RECIPE_SAVE_FAILURE,
} from '../../constants/newRecipe';
import { validateFields } from '../../Scenes/NewRecipe/Utils/utils';

const schemaIngredient = {
  amount: Joi.string().required(),
  name: Joi.string().required(),
  unit: Joi.string().allow(''),
};
const schemaItem = Joi.string().required();

const groupFields = {
  ingredients: {
    schema: schemaIngredient,
    defaultValue: { amount: '', name: '', unit: '' },
    defaultErrors: {},
    errorMessage: 'Required',
  },
  seasonings: { schema: schemaItem, defaultValue: '', defaultErrors: '' },
  steps: { schema: schemaItem, defaultValue: '', defaultErrors: '' },
};

export const initialState = {
  isSaving: false,
  didInvalidate: false,
  errors: {},
  value: {
    name: '',
    description: '',
    image: '',
    ingredients: [],
    seasonings: [],
    steps: [],
  },
  new: {
    ingredients: {
      id: 'new',
      status: 'new',
      value: groupFields.ingredients.defaultValue,
      errors: groupFields.ingredients.defaultErrors,
    },
    seasonings: {
      id: 'new',
      status: 'new',
      value: groupFields.seasonings.defaultValue,
      errors: groupFields.seasonings.defaultErrors,
    },
    steps: {
      id: 'new',
      status: 'new',
      value: groupFields.steps.defaultValue,
      errors: groupFields.steps.defaultErrors,
    },
  },
};

function handleGroupFieldChange(currentState, groupId, groupItem) {
  const groupField = groupFields[groupId];
  const { id, ...item } = groupItem;
  const nextState = cloneDeep(currentState);
  delete nextState.errors[groupId];

  if (!isNaN(id)) {
    const nextItems = cloneDeep(currentState.value[groupId]);
    nextItems[id] = {
      ...cloneDeep(item),
      errors: validateFields(
        groupField.schema, item.value, groupField.errorMessage) ||
        groupField.defaultErrors,
    };
    nextState.value[groupId] = nextItems;
    return nextState;
  }

  nextState.new[groupId] = {
    ...cloneDeep(item),
    errors: (nextState.new[groupId].status !== 'new'
      && validateFields(groupField.schema, item.value, groupField.errorMessage)
    ) || groupField.defaultErrors,
  };
  return nextState;
}

function handleNewItem(currentState, groupId) {
  const groupField = groupFields[groupId];
  const item = cloneDeep(currentState.new[groupId]);
  const nextState = cloneDeep(currentState);
  delete nextState.errors[groupId];
  const errors =
    validateFields(groupField.schema, item.value, groupField.errorMessage);

  if (errors) {
    item.errors = errors;
    item.status = 'editing';
    nextState.new[groupId] = item;
  } else {
    delete item.id;
    nextState.value[groupId].push({ ...item, status: 'done' });
    nextState.new[groupId] = {
      id: 'new',
      status: 'new',
      value: groupField.defaultValue,
      errors: groupField.defaultErrors,
    };
  }
  return nextState;
}

export default (state = initialState, action) => {
  switch (action.type) {
    case SINGLE_FIELD_CHANGE: {
      const nextState = cloneDeep(state);
      nextState.value[action.item.id] = action.item.value;
      delete nextState.errors[action.item.id];
      return nextState;
    }

    case GROUP_FIELD_CHANGE: {
      const { groupId, groupItem } = action;
      return handleGroupFieldChange(
        state,
        groupId,
        groupItem,
      );
    }

    case ADD_GROUP_FIELD: {
      return handleNewItem(state, action.groupId);
    }

    case EDIT_GROUP_FIELD: {
      const { groupId, id } = action;
      const nextState = cloneDeep(state);
      nextState.value[groupId][id].status = 'editing';
      return nextState;
    }

    case REMOVE_GROUP_FIELD: {
      const { groupId, id } = action;
      const nextState = cloneDeep(state);
      nextState.value[groupId].splice(id, 1);
      return nextState;
    }

    case DONE_GROUP_FIELD: {
      const { groupId, id } = action;

      if (id === 'new') {
        return handleNewItem(state, groupId);
      }

      const groupField = groupFields[groupId];
      const nextState = cloneDeep(state);
      const errors = validateFields(
        groupField.schema,
        nextState.value[groupId][id].value,
        groupField.errorMessage,
      );

      if (errors) {
        return state;
      }

      nextState.value[groupId][id].status = 'done';
      return nextState;
    }

    case NEW_RECIPE_SAVE_REQUEST:
      return {
        ...state,
        isSaving: true,
      };
    case NEW_RECIPE_SAVE_SUCCESS:
      return cloneDeep(initialState);
    case NEW_RECIPE_SAVE_FAILURE:
      return {
        ...state,
        isSaving: false,
        didInvalidate: true,
        errors: action.errors,
      };

    default:
      return state;
  }
};
