import React from 'react';
import PropTypes from 'prop-types';

import './FieldError.scss';

const FieldError = ({ children }) => (
  <span className="field-error">{children}</span>
);

FieldError.propTypes = {
  children: PropTypes.string.isRequired,
};

export default FieldError;
