import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import FormGroup from './FormGroup';
import Input from '../Input/Input';
import Textarea from '../Textarea/Textarea';
import Label from '../Label/Label';
import FieldError from '../FieldError/FieldError';

storiesOf('Components', module)
  .add('FormGroup: Label and Input', () => (
    <FormGroup>
      <Label htmlFor="input_name">Input Label</Label>
      <Input id="input_name" onChange={action('onChange-input')} placeholder="Input Name" />
    </FormGroup>
  ))
  .add('FormGroup: Label and Textare', () => (
    <FormGroup>
      <Label htmlFor="textarea_name">Textare Label</Label>
      <Textarea id="textarea_name" onChange={action('onChange-input')} placeholder="Textare Name" />
    </FormGroup>
  ))
  .add('FormGroup: Label, Input and Field Error', () => (
    <FormGroup>
      <Label htmlFor="textarea_name">Textare Label</Label>
      <Input id="input_name" onChange={action('onChange-input')} placeholder="Input Name" />
      <FieldError>This field is required</FieldError>
    </FormGroup>
  ))
  .add('FormGroup: Input and Field Error', () => (
    <FormGroup >
      <Input id="input_name" onChange={action('onChange-input')} placeholder="Input Name" />
      <FieldError>This field is required</FieldError>
    </FormGroup>
  ));
