import React from 'react';
import PropTypes from 'prop-types';

const DoneIcon = ({ width }) => (
  <svg width={width} height={width} xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" version="1.1" id="Capa_1" x="0px" y="0px" viewBox="0 0 50 50" style={{ enableBackground: 'new 0 0 50 50' }} xmlSpace="preserve">
    <circle style={{ fill: '#25AE88' }} cx="25" cy="25" r="25" />
    <polyline style={{ fill: 'none', stroke: '#FFFFFF', strokeWidth: 2, strokeLinecap: 'round', strokeLinejoin: 'round', strokeMiterlimit: 10 }} points="  38,15 22,33 12,25 " />
  </svg>
);

DoneIcon.propTypes = {
  width: PropTypes.number,
};

DoneIcon.defaultProps = {
  width: 'auto',
};

export default DoneIcon;
