import React from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import Figure from '../Figure/Figure';

import './FiguresList.scss';

const FiguresList = ({ figuresList, onClick }) => (
  <div className={classnames('figures-list', figuresList.length < 4 && ` figures-list--${figuresList.length}`)}>
    {
      figuresList.map(({ id, image, name }) => (
        <Figure key={id} id={id} src={image} caption={name} onClick={onClick} />
      ))
    }
  </div>
);

FiguresList.propTypes = {
  figuresList: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
  })).isRequired,
  onClick: PropTypes.func,
};

FiguresList.defaultProps = {
  onClick: null,
};

export default FiguresList;
