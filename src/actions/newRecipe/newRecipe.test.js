import axios from 'axios';
import cloneDeep from 'lodash.clonedeep';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import trueStore from '../../store';
import saveNewRecipe from './newRecipe';
import { initialState } from '../../reducers/newRecipe/newRecipe';
import {
  NEW_RECIPE_SAVE_REQUEST, NEW_RECIPE_SAVE_SUCCESS, NEW_RECIPE_SAVE_FAILURE,
} from '../../constants/newRecipe';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('axios');
jest.mock('../../store');

describe('Actions', () => {
  describe('newRecipe should', () => {
    axios.post.mockResolvedValue({ success: true });
    const rootState = { newRecipe: {} };
    let mockNewRecipe = {};
    let state = {};

    beforeEach(() => {
      state = cloneDeep(initialState);
      mockNewRecipe = {
        name: 'Recipe Name',
        description: 'Description text',
        image: 'image_uri',
        ingredients: [{ value: { amount: '12', name: 'Eggs', unit: '' } }],
        seasonings: [{ value: 'seasoning 1' }],
        steps: [{ value: 'step 1' }],
      };
      state.value = mockNewRecipe;
      rootState.newRecipe = state;
      trueStore.getState.mockReturnValue(rootState);
    });

    it(
      'invalidate newRecipe in case of missing name or description or image',
      async () => {
        mockNewRecipe.name = '';
        mockNewRecipe.description = '';
        mockNewRecipe.image = '';

        const expectedActions = [
          { type: NEW_RECIPE_SAVE_REQUEST },
          {
            type: NEW_RECIPE_SAVE_FAILURE,
            errors: {
              name: '"name" is not allowed to be empty',
              description: '"description" is not allowed to be empty',
              image: '"image" is not allowed to be empty',
            },
          },
        ];

        const store = mockStore({ newRecipe: state });

        await store.dispatch(saveNewRecipe());
        expect(store.getActions()).toEqual(expectedActions);
      },
    );

    it('invalidate newRecipe in case of missing ingredient', async () => {
      mockNewRecipe.ingredients[0].value.amount = '';

      const expectedActions = [
        { type: NEW_RECIPE_SAVE_REQUEST },
        {
          type: NEW_RECIPE_SAVE_FAILURE,
          errors: {
            ingredients: '"ingredients" is not allowed to be empty',
          },
        },
      ];
      const store = mockStore({ newRecipe: { ...initialState } });

      await store.dispatch(saveNewRecipe(mockNewRecipe));
      expect(store.getActions()).toEqual(expectedActions);
    });

    it('invalidate newRecipe in case of missing seasoning', async () => {
      mockNewRecipe.seasonings[0].value = '';

      const expectedActions = [
        { type: NEW_RECIPE_SAVE_REQUEST },
        {
          type: NEW_RECIPE_SAVE_FAILURE,
          errors: {
            seasonings: '"seasonings" is not allowed to be empty',
          },
        },
      ];
      const store = mockStore({ newRecipe: { ...initialState } });

      await store.dispatch(saveNewRecipe(mockNewRecipe));
      expect(store.getActions()).toEqual(expectedActions);
    });

    it('invalidate newRecipe in case of missing steps', async () => {
      mockNewRecipe.steps[0].value = '';

      const expectedActions = [
        { type: NEW_RECIPE_SAVE_REQUEST },
        {
          type: NEW_RECIPE_SAVE_FAILURE,
          errors: {
            steps: '"steps" is not allowed to be empty',
          },
        },
      ];
      const store = mockStore({ newRecipe: { ...initialState } });

      await store.dispatch(saveNewRecipe(mockNewRecipe));
      expect(store.getActions()).toEqual(expectedActions);
    });

    it('trigger success when get response', async () => {
      const expectedActions = [
        { type: NEW_RECIPE_SAVE_REQUEST },
        { type: NEW_RECIPE_SAVE_SUCCESS },
      ];
      const store = mockStore({ newRecipe: { ...initialState } });

      await store.dispatch(saveNewRecipe(mockNewRecipe));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
