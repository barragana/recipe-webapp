const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = require('./config');

module.exports = merge(config, {
  // the base path which will be used to resolve entry points
  context: path.join(__dirname, '../src'),
  // the main entry point for our application's frontend JS
  entry: [
    'webpack-hot-middleware/client?overlay=true',
    path.join(__dirname, '../src/index.js'),
  ],

  output: {
    // the filename of the compiled bundle, e.g. /assets/bundle.js
    filename: '[name].js',
  },
  mode: 'development',

  // set inline-source-map to re-built it whenever any file changes
  devtool: 'inline-source-map',

  plugins: [
    new webpack.optimize.OccurrenceOrderPlugin(),
    // activate hot reloading
    new webpack.HotModuleReplacementPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'),
    }),
    new HtmlWebpackPlugin({
      cache: false,
      inject: false,
      meta: { viewport: 'width=device-width,minimum-scale=1,initial-scale=1' },
      template: '../src/index.html',
      title: 'Recipe Amazing World',
    }),
  ],
});
