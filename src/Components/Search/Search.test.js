import React from 'react';
import { shallow, mount } from 'enzyme';

import Search from './Search';
import CleanIcon from '../SVGs/Clean';
import SearchIcon from '../SVGs/Search';

describe('Search', () => {
  describe('should', () => {
    const mockOnChange = jest.fn();

    afterEach(() => {
      mockOnChange.mockClear();
    });

    const wrapper = shallow(
      <Search
        list={['option 1', 'option 2', 'option 3', 'option 3 4', '3 option']}
        schema={item => (item)}
        onChange={mockOnChange}
        placeholder="placeholder text"
      />,
    );

    it(
      'render',
      () => {
        expect(wrapper.html()).not.toBeNull();
      },
    );
    it(
      'render input and icon',
      () => {
        expect(wrapper.find('input').exists()).toEqual(true);
        expect(wrapper.find(SearchIcon).exists()).toEqual(true);
      },
    );
    it(
      'render clear icon when value is not empty or typing',
      () => {
        const wrapperTyping = shallow(
          <Search
            list={
              ['option 1', 'option 2', 'option 3', 'option 3 4', '3 option']
            }
            schema={item => (item)}
            onChange={mockOnChange}
            placeholder="placeholder text"
            value="Deu"
          />,
        );
        expect(wrapperTyping.find(CleanIcon).exists()).toEqual(true);
      },
    );
  });
  describe('should', () => {
    const mockOnChange = jest.fn();

    const wrapperTyping = mount(
      <Search
        list={['option 1', 'option 2', 'option 3', 'option 3 4', '3 option']}
        schema={item => (item)}
        onChange={mockOnChange}
        placeholder="placeholder text"
      />,
    );

    afterEach(() => {
      mockOnChange.mockClear();
    });

    it(
      'render typed text into input',
      () => {
        const event = { target: { value: '3' } };
        let input = wrapperTyping.find('input');
        input.simulate('change', event);
        input = wrapperTyping.update().find('input');
        expect(input.props().value).toEqual('3');
      },
    );
    it(
      'call onChange after 500ms of stoping typing',
      (done) => {
        const event1 = { target: { value: '4' } };
        const input = wrapperTyping.find('input');
        input.simulate('change', event1);

        expect(mockOnChange).not.toBeCalled();

        setTimeout(() => {
          expect(mockOnChange).not.toBeCalled();
        }, 200);

        setTimeout(() => {
          expect(mockOnChange).not.toBeCalled();
        }, 400);

        setTimeout(() => {
          expect(mockOnChange).toBeCalledWith('4');
          done();
        }, 505);
      },
    );
    it(
      'call only once onChange after stoping typing',
      (done) => {
        const event1 = { target: { value: '3' } };
        const event2 = { target: { value: '4' } };
        const input = wrapperTyping.find('input');
        input.simulate('change', event1);

        expect(mockOnChange).not.toBeCalled();

        setTimeout(() => {
          input.simulate('change', event2);
          expect(mockOnChange).not.toBeCalled();
        }, 200);

        setTimeout(() => {
          expect(mockOnChange).not.toBeCalled();
        }, 510);

        setTimeout(() => {
          expect(mockOnChange).toBeCalledWith('4');
          done();
        }, 710);
      },
    );
  });
});
