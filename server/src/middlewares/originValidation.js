const debug = require('debug')('cors');
const cors = require('cors');

const corsOptions = {
  origin(origin, callback) {
    debug('Cors validation');
    debug('Origin: %s', origin);
    debug('Allowed origin: %s', process.env.ALLOWED_ORIGIN);

    if (process.env.NODE_ENV !== 'production') return callback(null, true);

    if (!origin || origin.match(process.env.ALLOWED_ORIGIN)) {
      return callback(null, true);
    }

    callback({ status: 401, message: 'Origin not allowed' });
  },
  credentials: true,
};

module.exports = () => cors(corsOptions);
