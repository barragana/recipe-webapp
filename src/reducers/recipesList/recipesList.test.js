import cloneDeep from 'lodash.clonedeep';
import reducer, { initialState } from './recipesList';
import { FILTER_RECIPE_LIST } from '../../constants/recipesList';

const mockRecipesList = [
  {
    id: 1,
    image: 'https://nutrition-assets.freeletics.com/recipes/large/mustard_deviled_eggs.jpg',
    name: 'Mustard Deviled Eggs',
    ingredients: [
      { name: 'ingredient 1' },
      { name: 'ingredient 2' },
      { name: 'ingredient 333' },
    ],
  },
  {
    id: 2,
    image: 'https://nutrition-assets.freeletics.com/recipes/large/feta_scrambled_eggs.jpg',
    name: 'Feta Scrambled Eggs',
    ingredients: [
      { name: 'ingredient 1' },
      { name: 'ingred333ient ' },
      { name: 'ingredient 0' },
    ],
  },
  {
    id: 3,
    image: 'https://nutrition-assets.freeletics.com/recipes/large/fried_fish_with_grapefruit_salsa.jpg',
    name: 'Fried Fish With Grapefruit Salsa',
    ingredients: [
      { name: 'ingredient 1' },
      { name: '333ingredient 6' },
      { name: 'ingredient 7' },
    ],
  },
  {
    id: 4,
    image: 'https://nutrition-assets.freeletics.com/recipes/large/classic_tuna_salad.jpg',
    name: 'Salade Niçoise',
    ingredients: [
      { name: 'ingredient 1' },
      { name: 'ingred4ient ' },
      { name: 'ingredient 5' },
    ],
  },
];

describe('Reducers', () => {
  describe('recipesList', () => {
    describe('FILTER_RECIPE_LIST', () => {
      describe('should', () => {
        it(
          'filter recipe list according action term',
          () => {
            const action = {
              type: FILTER_RECIPE_LIST,
              term: '333',
            };

            initialState.value = mockRecipesList;
            const state = cloneDeep(initialState);
            const nextState = reducer(state, action);

            expect(nextState.search.value).toEqual([
              mockRecipesList[0], mockRecipesList[1], mockRecipesList[2],
            ]);
            expect(nextState.search.text).toEqual('333');
            expect(nextState.search.isSearching).toEqual(true);
            expect(state).toEqual(initialState);
          },
        );

        it(
          'return empty when filtering recipe list term is not found',
          () => {
            const action = {
              type: FILTER_RECIPE_LIST,
              term: '999',
            };

            initialState.value = mockRecipesList;
            const state = cloneDeep(initialState);
            const nextState = reducer(state, action);

            expect(nextState.search.value).toEqual([]);
            expect(nextState.search.text).toEqual('999');
            expect(nextState.search.isSearching).toEqual(true);
            expect(state).toEqual(initialState);
          },
        );

        it(
          'return full list when term is empty',
          () => {
            const action = {
              type: FILTER_RECIPE_LIST,
              term: '',
            };

            initialState.value = mockRecipesList;
            const state = cloneDeep(initialState);
            const nextState = reducer(state, action);

            expect(nextState.search.value).toEqual(mockRecipesList);
            expect(nextState.search.text).toEqual('');
            expect(nextState.search.isSearching).toEqual(false);
            expect(state).toEqual(initialState);
          },
        );
      });
    });
  });
});
