import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Field from './Field';

storiesOf('Containers', module)
  .add('Field: Input', () => (
    <Field id="input_name" type="input" onChange={action('onChange-input')}>
      Input Field
    </Field>
  ))
  .add('Field: Input disabled', () => (
    <Field id="input_name" type="input" onChange={action('onChange-input')} disabled>
      Input Field
    </Field>
  ))
  .add('Field: Input error', () => (
    <Field id="input_name" type="input" onChange={action('onChange-input')} error="This field is required">
      Input Field
    </Field>
  ))
  .add('Field: Textare', () => (
    <Field id="input_name" type="textarea" onChange={action('onChange-input')}>
      Input Field
    </Field>
  ));
