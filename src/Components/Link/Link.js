import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import './Link.scss';

const CustomLink = ({ children, ...props }) => (
  <Link className="link" {...props}>
    {children}
  </Link>
);

CustomLink.propTypes = {
  children: PropTypes.node.isRequired,
};

export default CustomLink;
