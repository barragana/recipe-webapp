import React from 'react';
import { storiesOf } from '@storybook/react';

import Seasoning from './Seasoning';

storiesOf('RecipeDetails', module)
  .add('Seasoning', () => (
    <Seasoning name="Paprika powder" />
  ));
