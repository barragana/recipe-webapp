import React from 'react';
import { shallow } from 'enzyme';

import Textarea from './Textarea';

describe('Components', () => {
  describe('Textarea', () => {
    describe('should', () => {
      const mockOnChange = jest.fn();

      const wrapper = shallow(
        <Textarea id="name" onChange={mockOnChange} />,
      );

      it(
        'render',
        () => {
          expect(wrapper.html()).not.toBeNull();
        },
      );

      it(
        'trigger onChange when textare value change',
        () => {
          const textarea = wrapper.find('textarea');
          expect(textarea.exists()).toEqual(true);

          textarea.simulate('change', {
            target: { id: 'name', value: 'My new value' },
          });
          expect(mockOnChange).toBeCalled();
          expect(mockOnChange).toBeCalledWith({
            target: { id: 'name', value: 'My new value' },
          });
        },
      );
    });
  });
});
