import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';

import recipesList from './recipesList/recipesList';
import recipeDetails from './recipeDetails';
import newRecipe from './newRecipe/newRecipe';

export default combineReducers({
  routing: routerReducer,
  recipesList,
  recipeDetails,
  newRecipe,
});
