import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import NewItems from './NewItems';

const mockData = {
  items: [],
  new: {
    id: 'new',
    status: 'new',
    value: '',
    errors: '',
  },
};

storiesOf('NewRecipe/Components', module)
  .add('NewItems', () => (
    <NewItems items={mockData.items} newItem={mockData.new} onActionClick={action('actions-click')} onFieldChange={action('filed-change')} />
  ));
