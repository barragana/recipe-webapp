import React from 'react';
import PropTypes from 'prop-types';

import Ingredient from '../Ingredient/Ingredient';

import './IngredientsList.scss';

const IngredientsList = ({ ingredientsList }) => (
  <div className="ingredients-list">
    {
      ingredientsList.map(({ amount, name, unit }, key) => (
        <Ingredient
          key={`ingredient_${key}`}
          amount={amount}
          name={name}
          unit={unit}
        />
      ))
    }
  </div>
);

IngredientsList.propTypes = {
  ingredientsList: PropTypes.arrayOf(PropTypes.shape({
    amount:
      PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    name: PropTypes.string.isRequired,
    unit: PropTypes.string.isRequired,
  })).isRequired,
};

export default IngredientsList;
