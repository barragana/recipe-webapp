import React from 'react';
import { shallow } from 'enzyme';

import Button from './Button';

describe('Components', () => {
  describe('Button', () => {
    const mockOnClick = jest.fn();

    const wrapper = shallow(
      <Button onClick={mockOnClick}>
        Text
      </Button>,
    );

    it(
      'render',
      () => {
        expect(wrapper.html()).not.toBeNull();
      },
    );
    it(
      'trigger mockOnClick when button is clicked',
      () => {
        const button = wrapper.find('button');
        button.simulate('click');
        expect(mockOnClick).toBeCalled();
      },
    );
  });
});
