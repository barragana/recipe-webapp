import cloneDeep from 'lodash.clonedeep';
import reducer, { initialState } from './newRecipe';
import {
  SINGLE_FIELD_CHANGE, GROUP_FIELD_CHANGE, ADD_GROUP_FIELD, EDIT_GROUP_FIELD,
  REMOVE_GROUP_FIELD, DONE_GROUP_FIELD,
} from '../../constants/newRecipe';

describe('Reducers', () => {
  describe('newRecipe', () => {
    describe('SINGLE_FIELD_CHANGE', () => {
      describe('should', () => {
        it(
          'update field according id and value of action.item',
          () => {
            const action1 = {
              type: SINGLE_FIELD_CHANGE,
              item: { id: 'name', value: 'Recipe Name' },
            };

            const state = cloneDeep(initialState);
            let nextState = reducer(state, action1);
            expect(nextState.value.name).toEqual(action1.item.value);
            expect(state).toEqual(initialState);

            const action2 = {
              type: SINGLE_FIELD_CHANGE,
              item: { id: 'description', value: 'Recipe Description' },
            };
            nextState = reducer(nextState, action2);
            expect(nextState.value.name).toEqual(action1.item.value);
            expect(nextState.value.description).toEqual(action2.item.value);
          },
        );
      });
    });
    describe('GROUP_FIELD_CHANGE', () => {
      describe('should', () => {
        it(
          'update new group field according groupId and groupItem of action',
          () => {
            const action1 = {
              type: GROUP_FIELD_CHANGE,
              groupId: 'ingredients',
              groupItem: {
                id: 'new',
                status: 'new',
                value: { amount: '12', name: 'Eggs', unit: '' },
                errors: {},
              },
            };

            const state = cloneDeep(initialState);
            const nextState = reducer(state, action1);
            expect(nextState.new.ingredients).toEqual({
              status: 'new',
              value: { amount: '12', name: 'Eggs', unit: '' },
              errors: {},
            });
            expect(state).toEqual(initialState);
          },
        );

        it(
          `update existing group field according groupId and
              groupItem of action`,
          () => {
            const action1 = {
              type: GROUP_FIELD_CHANGE,
              groupId: 'ingredients',
              groupItem: {
                id: '0',
                status: 'new',
                value: { amount: '5', name: 'Eggs', unit: '' },
                errors: {},
              },
            };

            const state = cloneDeep(initialState);
            state.value.ingredients[0] = {
              status: 'done',
              value: { amount: '12', name: 'Eggs', unit: '' },
              errors: {},
            };
            const nextState = reducer(state, action1);
            expect(nextState.new.ingredients).toEqual({
              id: 'new',
              status: 'new',
              value: { amount: '', name: '', unit: '' },
              errors: {},
            });
            expect(nextState.value.ingredients[0].value).toEqual({
              amount: '5', name: 'Eggs', unit: '',
            });
            expect(state.value.ingredients[0].value).toEqual({
              amount: '12', name: 'Eggs', unit: '',
            });
          },
        );

        it(
          'have errors when group field editing is empty',
          () => {
            const action1 = {
              type: GROUP_FIELD_CHANGE,
              groupId: 'ingredients',
              groupItem: {
                id: '0',
                status: 'editing',
                value: { amount: '', name: 'Eggs', unit: '' },
                errors: {},
              },
            };

            const state = cloneDeep(initialState);
            state.value.ingredients[0] = {
              status: 'done',
              value: { amount: '12', name: 'Eggs', unit: '' },
              errors: {},
            };
            const nextState = reducer(state, action1);
            expect(nextState.value.ingredients[0].value).toEqual({
              amount: '', name: 'Eggs', unit: '',
            });
            expect(nextState.value.ingredients[0].errors).toEqual({
              amount: 'Required',
            });
            expect(state.value.ingredients[0].value).toEqual({
              amount: '12', name: 'Eggs', unit: '',
            });
          },
        );
      });
    });
    describe('ADD_GROUP_FIELD', () => {
      describe('should', () => {
        it(
          'reset new object of the specific groupId',
          () => {
            const action1 = {
              type: ADD_GROUP_FIELD,
              groupId: 'ingredients',
            };

            const state = cloneDeep(initialState);
            state.new.ingredients = {
              id: 'new',
              status: 'new',
              value: { amount: '5', name: 'Eggs', unit: '' },
              errors: {},
            };
            const imutableState = cloneDeep(state);
            const nextState = reducer(state, action1);

            expect(nextState.new.ingredients)
              .toEqual(initialState.new.ingredients);
            expect(state).toEqual(imutableState);
          },
        );

        it(
          'add new object to the specific value groupId',
          () => {
            const action1 = {
              type: ADD_GROUP_FIELD,
              groupId: 'ingredients',
            };
            const state = cloneDeep(initialState);
            state.new.ingredients = {
              id: 'new',
              status: 'new',
              value: { amount: '5', name: 'Eggs', unit: '' },
              errors: {},
            };
            const imutableState = cloneDeep(state);
            const nextState = reducer(state, action1);

            expect(nextState.value.ingredients[0]).toEqual({
              status: 'done',
              value: { amount: '5', name: 'Eggs', unit: '' },
              errors: {},
            });
            expect(state).toEqual(imutableState);
          },
        );

        it(
          'have error when try to add group field with required field empty',
          () => {
            const action1 = {
              type: ADD_GROUP_FIELD,
              groupId: 'ingredients',
            };
            const state = cloneDeep(initialState);
            state.new.ingredients = {
              id: 'new',
              status: 'new',
              value: { amount: '', name: 'Eggs', unit: '' },
              errors: {},
            };
            const imutableState = cloneDeep(state);
            const nextState = reducer(state, action1);

            expect(nextState.value.ingredients.length).toEqual(0);
            expect(nextState.new.ingredients).toEqual({
              id: 'new',
              status: 'editing',
              value: { amount: '', name: 'Eggs', unit: '' },
              errors: { amount: 'Required' },
            });
            expect(state).toEqual(imutableState);
          },
        );
      });
    });
    describe('EDIT_GROUP_FIELD', () => {
      describe('should', () => {
        it(
          'change object of the specific groupId to editing',
          () => {
            const action1 = {
              type: EDIT_GROUP_FIELD,
              groupId: 'seasonings',
              id: '1',
            };

            const state = cloneDeep(initialState);
            state.value.seasonings = [
              {
                status: 'done',
                value: 'seasonings 0',
                errors: {},
              },
              {
                status: 'done',
                value: 'seasonings 1',
                errors: {},
              },
            ];

            const imutableState = cloneDeep(state);
            const nextState = reducer(state, action1);

            expect(nextState.value.seasonings[1].status).toEqual('editing');
            expect(state).toEqual(imutableState);
          },
        );
      });
    });
    describe('REMOVE_GROUP_FIELD', () => {
      describe('should', () => {
        it(
          'remove object of the specific groupId',
          () => {
            const action1 = {
              type: REMOVE_GROUP_FIELD,
              groupId: 'seasonings',
              id: '0',
            };

            const state = cloneDeep(initialState);
            state.value.seasonings = [
              {
                status: 'done',
                value: 'seasonings 0',
                errors: {},
              },
              {
                status: 'done',
                value: 'seasonings 1',
                errors: {},
              },
            ];

            expect(state.value.seasonings.length).toEqual(2);

            const imutableState = cloneDeep(state);
            const nextState = reducer(state, action1);

            expect(nextState.value.seasonings.length).toEqual(1);
            expect(nextState.value.seasonings[0]).toEqual({
              status: 'done',
              value: 'seasonings 1',
              errors: {},
            });
            expect(state).toEqual(imutableState);
          },
        );
      });
    });
    describe('DONE_GROUP_FIELD', () => {
      describe('should', () => {
        it(
          'change object status of the specific groupId to done',
          () => {
            const action1 = {
              type: DONE_GROUP_FIELD,
              groupId: 'seasonings',
              id: '1',
            };

            const state = cloneDeep(initialState);
            state.value.seasonings = [
              {
                status: 'done',
                value: 'seasonings 0',
                errors: {},
              },
              {
                status: 'editing',
                value: 'seasonings 1',
                errors: {},
              },
            ];

            const imutableState = cloneDeep(state);
            const nextState = reducer(state, action1);

            expect(nextState.value.seasonings[1].status).toEqual('done');
            expect(state).toEqual(imutableState);
          },
        );

        it(
          'keep object the same when there is errors',
          () => {
            const action1 = {
              type: DONE_GROUP_FIELD,
              groupId: 'seasonings',
              id: '1',
            };

            const state = cloneDeep(initialState);
            state.value.seasonings = [
              {
                status: 'done',
                value: 'seasonings 0',
                errors: '',
              },
              {
                status: 'editing',
                value: '',
                errors: '',
              },
            ];

            const imutableState = cloneDeep(state);
            const nextState = reducer(state, action1);

            expect(nextState.value.seasonings[1].status).toEqual('editing');
            expect(state).toEqual(imutableState);
          },
        );

        it(
          'add new object to the specific groupId',
          () => {
            const action1 = {
              type: DONE_GROUP_FIELD,
              groupId: 'seasonings',
              id: 'new',
            };

            const state = cloneDeep(initialState);
            state.new.seasonings = {
              id: 'new',
              status: 'editing',
              value: 'seasonings new',
              errors: '',
            };

            const imutableState = cloneDeep(state);
            const nextState = reducer(state, action1);

            expect(nextState.value.seasonings[0]).toEqual({
              status: 'done',
              value: 'seasonings new',
              errors: '',
            });
            expect(state).toEqual(imutableState);
          },
        );
      });
    });
  });
});
