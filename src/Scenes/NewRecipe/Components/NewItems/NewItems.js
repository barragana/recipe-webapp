import React from 'react';
import PropTypes from 'prop-types';

import Actions from '../../Components/Actions/Actions';
import Field from '../../../../Containers/Field/Field';

import './NewItems.scss';

const NewItems = ({ items, newItem, onActionClick, onFieldChange }) => (
  <div className="new-items">
    {
      items.map((item, key) => (
        <div className="new-ingredients__item" key={`new-ingredient__item--${key}`}>
          <Field id={key} onChange={onFieldChange} disabled={item.status === 'done'} value={item.value} error={item.errors} />
          <Actions id={key} status={item.status} onClick={onActionClick} />
        </div>
      ))
    }
    <div className="new-items__item">
      <Field id="new" onChange={onFieldChange} value={newItem.value} error={newItem.errors} />
      <Actions id="new" status={newItem.status} onClick={onActionClick} />
    </div>
  </div>
);

NewItems.propTypes = {
  items: PropTypes.array.isRequired,
  newItem: PropTypes.object.isRequired,
  onActionClick: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired,
};

export default NewItems;
