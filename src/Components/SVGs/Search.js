import React from 'react';

const Search = () => (
  <svg viewBox="0 0 11 11" width="11" height="11" xmlns="http://www.w3.org/2000/svg">
    <circle cx="4.5" cy="4.5" r="4" stroke="#807877" fill="#ffffff" strokeWidth="1" />
    <line x1="7" y1="7" x2="11" y2="11" strokeWidth="1" stroke="#807877" />
  </svg>
);

export default Search;
