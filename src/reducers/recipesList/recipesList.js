import {
  RECIPES_LIST_REQUEST, RECIPES_LIST_SUCCESS, RECIPES_LIST_FAILURE,
  RECIPE_FIND_REQUEST, RECIPE_FIND_SUCCESS, RECIPE_FIND_FAILURE,
  FILTER_RECIPE_LIST,
} from '../../constants/recipesList';

export const initialState = {
  isFetching: false,
  didInvalidate: false,
  value: [],
  search: {
    value: [],
    text: '',
    isSearching: false,
  },
};

export default (state = initialState, action) => {
  switch (action.type) {
    case RECIPES_LIST_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case RECIPES_LIST_SUCCESS:
      return {
        ...state,
        value: action.recipesList,
        isFetching: false,
      };
    case RECIPES_LIST_FAILURE:
      return {
        ...state,
        error: action.err.message,
        isFetching: false,
        didInvalidate: true,
      };
    case RECIPE_FIND_REQUEST:
      return {
        ...state,
        isFetching: true,
      };
    case RECIPE_FIND_SUCCESS:
      return {
        ...state,
        value: action.recipesList,
        isFetching: false,
      };
    case RECIPE_FIND_FAILURE:
      return {
        ...state,
        isFetching: false,
        didInvalidate: true,
      };
    case FILTER_RECIPE_LIST: {
      if (!action.term) {
        return {
          ...state,
          search: {
            value: state.value,
            text: action.term,
            isSearching: false,
          },
        };
      }

      const filteredList = state.value.filter(recipe => (
        recipe.ingredients.some((ingredient) => {
          const regex = new RegExp(`${action.term.toLowerCase()}.*`);
          return regex.test(ingredient.name.toLowerCase());
        })
      ));

      return {
        ...state,
        search: {
          value: filteredList,
          text: action.term,
          isSearching: true,
        },
      };
    }
    default:
      return state;
  }
};
