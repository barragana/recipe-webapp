import React from 'react';
import { storiesOf } from '@storybook/react';

import Step from './Step';

storiesOf('RecipeDetails', module)
  .add('Step', () => (
    <Step number={2} description="Fill egg halves with yolk mixture and garnish with remaining dill." />
  ));
