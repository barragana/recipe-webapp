import React from 'react';
import { shallow } from 'enzyme';

import Input from './Input';

describe('Components', () => {
  describe('Input', () => {
    describe('should', () => {
      const mockOnChange = jest.fn();

      const wrapper = shallow(
        <Input id="name" onChange={mockOnChange} />,
      );

      it(
        'render',
        () => {
          expect(wrapper.html()).not.toBeNull();
        },
      );

      it(
        'trigger onChange when input value change',
        () => {
          const input = wrapper.find('input');
          expect(input.exists()).toEqual(true);

          input.simulate('change', {
            target: { id: 'name', value: 'My new value' },
          });
          expect(mockOnChange).toBeCalled();
          expect(mockOnChange).toBeCalledWith({
            target: { id: 'name', value: 'My new value' },
          });
        },
      );
    });
  });
});
