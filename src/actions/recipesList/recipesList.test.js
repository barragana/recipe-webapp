import axios from 'axios';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import requestRecipesList from './recipesList';
import { initialState } from '../../reducers/recipesList/recipesList';
import {
  RECIPES_LIST_REQUEST, RECIPES_LIST_SUCCESS,
} from '../../constants/recipesList';

const mockRecipesList = { data:
  [
    { id: 0, name: 'Recipe 1' },
    { id: 1, name: 'Recipe 2' },
    { id: 2, name: 'Recipe 3' },
    { id: 3, name: 'Recipe 4' },
  ],
};

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('axios');

describe('Actions', () => {
  describe('recipesList should', () => {
    it('trigger the correct actions and receive a list', async () => {
      axios.get.mockResolvedValue(mockRecipesList);

      const expectedActions = [
        { type: RECIPES_LIST_REQUEST },
        { type: RECIPES_LIST_SUCCESS, recipesList: mockRecipesList.data },
      ];
      const store = mockStore({ recipesList: { ...initialState } });

      await store.dispatch(requestRecipesList());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
