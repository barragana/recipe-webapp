import React from 'react';
import PropTypes from 'prop-types';

import './Seasoning.scss';

const Seasoning = ({ name }) => (
  <span className="seasoning">
    {name}
  </span>
);

Seasoning.propTypes = {
  name: PropTypes.string.isRequired,
};

export default Seasoning;
