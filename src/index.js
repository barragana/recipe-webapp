/* eslint-disable global-require */
import React from 'react';
import { render } from 'react-dom';

import App from './App';

function initialize(Component) {
  render(
    <Component />,
    document.querySelector('#root'),
  );
}

initialize(App);

if (module.hot) {
  module.hot.accept('./App', () => {
    const NextApp = require('./App').default;
    initialize(NextApp);
  });
}
