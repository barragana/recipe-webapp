import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Textarea from './Textarea';

storiesOf('Components', module)
  .add('Textarea', () => (
    <Textarea id="input_name" onChange={action('input-change')} placeholder="textare" />
  ));
