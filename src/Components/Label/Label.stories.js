import React from 'react';
import { storiesOf } from '@storybook/react';

import Label from './Label';

storiesOf('Components', module)
  .add('Label', () => (
    <Label htmlFor="input_name">
      Input Label
    </Label>
  ));
