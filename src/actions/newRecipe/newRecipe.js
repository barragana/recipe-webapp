import axios from 'axios';
import Joi from 'joi';

import { validateRecipe } from '../../Scenes/NewRecipe/Utils/utils';
import store from '../../store';

import {
  SINGLE_FIELD_CHANGE, GROUP_FIELD_CHANGE, NEW_RECIPE_SAVE_REQUEST,
  NEW_RECIPE_SAVE_SUCCESS, NEW_RECIPE_SAVE_FAILURE,
} from '../../constants/newRecipe';

export function singleFieldChange(item) {
  return {
    type: SINGLE_FIELD_CHANGE,
    item,
  };
}

export function groupFieldChange(groupId, groupItem) {
  return {
    type: GROUP_FIELD_CHANGE,
    groupId,
    groupItem,
  };
}

export function clickActionToGroupField(type, { groupId, id }) {
  return { type, groupId, id };
}

export function newRecipeSaveRequest() {
  return {
    type: NEW_RECIPE_SAVE_REQUEST,
  };
}

export function newRecipeSaveSuccess() {
  return {
    type: NEW_RECIPE_SAVE_SUCCESS,
  };
}

export function newRecipeSaveFailure(errors) {
  return {
    type: NEW_RECIPE_SAVE_FAILURE,
    errors,
  };
}

const schemaIngredient = Joi.object().keys({
  amount: Joi.string().required(),
  name: Joi.string().required(),
  unit: Joi.string().allow(''),
});

const schema = {
  name: Joi.string().required(),
  description: Joi.string().required(),
  image: {
    uri: Joi.string().required(),
    type: Joi.string().required(),
  },
  ingredients: Joi.array().items(schemaIngredient).min(1),
  seasonings: Joi.array().items(Joi.string()).min(1),
  steps: Joi.array().items(Joi.string()).min(1),
};

export default () => (
  (dispatch) => {
    dispatch(newRecipeSaveRequest());

    const newRecipeState = store.getState().newRecipe.value;
    const newRecipe = {
      name: newRecipeState.name,
      description: newRecipeState.description,
      image: {
        uri: newRecipeState.image,
        type: 'image/jpeg',
      },
      ingredients:
        newRecipeState.ingredients.map(ingredient => ingredient.value),
      seasonings: newRecipeState.seasonings.map(seasoning => seasoning.value),
      steps: newRecipeState.steps.map(step => step.value),
    };

    const errors = validateRecipe(schema, newRecipe);
    if (errors) {
      return dispatch(newRecipeSaveFailure(errors));
    }

    axios.post(
      'https://private-anon-efe5ea8df6-reactnativemockapi.apiary-mock.com/recipes',
      newRecipe,
    )
      .then(
        () => { dispatch(newRecipeSaveSuccess()); },
        (e) => {
          dispatch(newRecipeSaveFailure(e));
        },
      );
  }
);
