import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Field from '../../../../Containers/Field/Field';

import './NewIngredient.scss';

class NewIngredient extends Component {
  constructor(props) {
    super(props);
    this.state = { ...props.ingredient };

    this.handleChange = this.handleChange.bind(this);
  }

  componentWillReceiveProps({ ingredient }) {
    this.setState({ ...ingredient });
  }

  handleChange({ id, value }) {
    const nextState = { ...this.state };
    nextState.value[id] = value;

    this.props.onChange(nextState);

    this.setState(nextState);
  }

  render() {
    const { disabled } = this.props;
    const { amount, name, unit } = this.state.value;
    const { errors } = this.state;

    return (
      <div className="new-ingredient">
        <div className="new-ingredient__amount">
          <Field id="amount" placeholder="Amount" onChange={this.handleChange} disabled={disabled} value={amount} error={errors.amount && 'Required'} />
        </div>
        <div className="new-ingredient__name">
          <Field id="name" placeholder="Name" onChange={this.handleChange} disabled={disabled} value={name} error={errors.name && 'Required'} />
        </div>
        <div className="new-ingredient__unit">
          <Field id="unit" placeholder="Unit" onChange={this.handleChange} disabled={disabled} value={unit} error={errors.unit} />
        </div>
      </div>
    );
  }
}

NewIngredient.propTypes = {
  disabled: PropTypes.bool,
  ingredient: PropTypes.shape({
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    status: PropTypes.string,
    value: PropTypes.shape({
      amount: PropTypes.string.isRequired,
      name: PropTypes.string.isRequired,
      unit: PropTypes.string,
    }),
    errors: PropTypes.shape({
      amount: PropTypes.string,
      name: PropTypes.string,
      unit: PropTypes.string,
    }),
  }),
  onChange: PropTypes.func.isRequired,
};

NewIngredient.defaultProps = {
  disabled: false,
  ingredient: {
    id: 'new',
    status: 'new',
    value: {
      amount: '',
      name: '',
      unit: '',
    },
    errors: {
      amount: '',
      name: '',
      unit: '',
    },
  },
};

export default NewIngredient;
