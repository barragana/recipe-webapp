import React from 'react';
import PropTypes from 'prop-types';

import './RecipeDetails.scss';
import IngredientsIcon from '../../../Components/SVGs/Ingredients';
import Seasonings from '../../../Components/SVGs/Seasonings';
import IngredientsList from './IngredientsList/IngredientsList';
import SeasoningsList from './SeasoningsList/SeasoningsList';
import StepsList from './StepsList/StepsList';

const RecipeDetails = ({ details }) => {
  const { description, image, name, ingredients, seasonings, steps } = details;
  return (
    <main className="recipe-details">
      <div className="recipe-details__img--crop">
        <img className="recipe-details__img" src={image} alt={`recipe ${name}`} width="100%" height="auto" />
      </div>
      <div className="recipe-details__container recipe-details--main recipe-details--margin-bottom recipe-details--padding-horizontal">
        <h2 className="recipe-details__header recipe-details--border-bottom">{name}</h2>
        <p className="recipe-details__description">{description}</p>
      </div>
      <div className="recipe-details__container recipe-details--secondary recipe-details--margin-bottom">
        <div className="recipe-details__ingredients">
          <div className="recipe-details--padding-horizontal">
            <div className="recipe-details__header--flex recipe-details--border-bottom">
              <h4 className="recipe-details__header ">
                Ingredients
              </h4>
              <IngredientsIcon width={30} />
            </div>
            <IngredientsList ingredientsList={ingredients} />
          </div>
        </div>
        <div className="recipe-details__seasonings">
          <div className="recipe-details--padding-horizontal">
            <div className="recipe-details__header--flex">
              <h4 className="recipe-details__header ">
                Seasonings
              </h4>
              <Seasonings width={30} />
            </div>
            <SeasoningsList seasoningsList={seasonings} />
          </div>
        </div>
      </div>
      <div className="recipe-details__container recipe-details--padding-horizontal">
        <div className="recipe-details__steps">
          <h3 className="recipe-details__header recipe-details--border-bottom">
            Instructions
          </h3>
          <StepsList stepsList={steps} />
        </div>
      </div>
    </main>
  );
};

RecipeDetails.propTypes = {
  details: PropTypes.shape({
    description: PropTypes.string.isRequired,
    id: PropTypes.number.isRequired,
    image: PropTypes.string.isRequired,
    ingredients: PropTypes.arrayOf(PropTypes.shape({
      amount:
        PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
      name: PropTypes.string.isRequired,
      unit: PropTypes.string.isRequired,
    })),
    name: PropTypes.string.isRequired,
    seasonings: PropTypes.arrayOf(PropTypes.string),
    steps: PropTypes.arrayOf(PropTypes.string),
  }).isRequired,
};


export default RecipeDetails;
