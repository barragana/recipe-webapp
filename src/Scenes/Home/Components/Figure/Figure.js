import React, { Component } from 'react';
import PropTypes from 'prop-types';

import FlatButton from '../../../../Components/FlatButton/FlatButton';

import './Figure.scss';

class Figure extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.onClick(this.props.id);
  }

  render() {
    const { src, caption } = this.props;

    return (
      <figure className="figure">
        <FlatButton onClick={this.handleClick}>
          <div className="figure--crop">
            <img className="figure__img" src={src} alt={`recipe ${caption}`} width="100%" height="auto" />
          </div>
          <figcaption>
            <p className="figure__caption">{caption}</p>
          </figcaption>
        </FlatButton>
      </figure>
    );
  }
}

Figure.propTypes = {
  id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  caption: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  src: PropTypes.string.isRequired,
};

Figure.defaultProps = {
  id: null,
  onClick: null,
};

export default Figure;
