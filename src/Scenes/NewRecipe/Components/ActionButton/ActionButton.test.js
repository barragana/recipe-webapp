import React from 'react';
import { shallow } from 'enzyme';

import ActionButton from './ActionButton';
import FlatButton from '../../../../Components/FlatButton/FlatButton';
import EditIcon from '../../../../Components/SVGs/Edit';
import RemoveIcon from '../../../../Components/SVGs/Remove';
import AddIcon from '../../../../Components/SVGs/Add';

describe('NewRecipe', () => {
  describe('Components', () => {
    describe('ActionButton', () => {
      describe('should', () => {
        it(
          'render',
          () => {
            const mockOnClick = jest.fn();
            const wrapper = shallow(
              <ActionButton
                action="edit"
                type="input"
                onClick={mockOnClick}
              />,
            );
            expect(wrapper.html()).not.toBeNull();
          },
        );
        it(
          'render EditIcon when edit action',
          () => {
            const mockOnClick = jest.fn();
            const wrapper = shallow(
              <ActionButton
                action="edit"
                type="input"
                onClick={mockOnClick}
              />,
            );
            expect(wrapper.find(EditIcon).exists()).toEqual(true);
          },
        );
        it(
          'render AddIcon when add action',
          () => {
            const mockOnClick = jest.fn();
            const wrapper = shallow(
              <ActionButton
                action="add"
                type="input"
                onClick={mockOnClick}
              />,
            );
            expect(wrapper.find(AddIcon).exists()).toEqual(true);
          },
        );
        it(
          'render RemoveIcon when remove action',
          () => {
            const mockOnClick = jest.fn();
            const wrapper = shallow(
              <ActionButton
                action="remove"
                type="input"
                onClick={mockOnClick}
              />,
            );
            expect(wrapper.find(RemoveIcon).exists()).toEqual(true);
          },
        );
        it(
          'trigger mockOnClick with correspoding action when clicked',
          () => {
            const mockOnClick = jest.fn();
            const wrapper = shallow(
              <ActionButton
                action="remove"
                type="input"
                onClick={mockOnClick}
              />,
            );

            wrapper.find(FlatButton).simulate('click');
            expect(mockOnClick).toBeCalled();
            expect(mockOnClick).toBeCalledWith('remove');
          },
        );
      });
    });
  });
});
