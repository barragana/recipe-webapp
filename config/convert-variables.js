const scssToJson = require('scss-to-json');
const path = require('path');
const fs = require('fs');

const variables = scssToJson(
  path.resolve(__dirname, '..', 'src', 'styles', '_variables.scss'));

fs.writeFileSync(
  path.resolve(__dirname, '..', 'lib', '_variables.js'),
  `module.exports = ${JSON.stringify(variables, null, 2)}`);
