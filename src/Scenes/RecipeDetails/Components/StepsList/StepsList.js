import React from 'react';
import PropTypes from 'prop-types';

import Step from '../Step/Step';

import './StepsList.scss';

const StepsList = ({ stepsList }) => (
  <div className="steps-list">
    {
      stepsList.map((step, number) => (
        <Step key={`step_${number}`} number={number} description={step} />
      ))
    }
  </div>
);


StepsList.propTypes = {
  stepsList: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default StepsList;
