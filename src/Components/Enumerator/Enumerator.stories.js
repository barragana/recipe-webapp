import React from 'react';
import { storiesOf } from '@storybook/react';

import Enumerator from './Enumerator';

storiesOf('Components', module)
  .add('Enumerator', () => (
    <Enumerator number={2} />
  ));
