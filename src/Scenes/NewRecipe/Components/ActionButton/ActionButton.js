import React, { Component } from 'react';
import PropTypes from 'prop-types';

import FlatButton from '../../../../Components/FlatButton/FlatButton';

import EditIcon from '../../../../Components/SVGs/Edit';
import RemoveIcon from '../../../../Components/SVGs/Remove';
import AddIcon from '../../../../Components/SVGs/Add';
import DoneIcon from '../../../../Components/SVGs/Done';

import './ActionButton.scss';

const Icons = {
  add: AddIcon,
  done: DoneIcon,
  edit: EditIcon,
  remove: RemoveIcon,
};

class ActionButton extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    this.props.onClick(this.props.action);
  }

  render() {
    const Icon = Icons[this.props.action];

    return (
      <div className="action-button">
        <FlatButton onClick={this.handleClick}>
          <Icon width={20} />
        </FlatButton>
      </div>
    );
  }
}

ActionButton.propTypes = {
  action: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default ActionButton;
