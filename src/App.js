import React from 'react';
import { ConnectedRouter } from 'react-router-redux';
import { Route } from 'react-router-dom';
import { Provider } from 'react-redux';
import store, { history } from './store';

import Base from './Containers/Base/Base';
import Home from './Scenes/Home/Home';
import RecipeDetails from './Scenes/RecipeDetails/RecipeDetailsContainer';
import NewRecipe from './Scenes/NewRecipe/NewRecipe';

export default () => (
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <Base>
        <Route exact path="/recipes/:id(\d+)" component={RecipeDetails} types={{ id: Number }} />
        <Route exact path="/recipes/new" component={NewRecipe} />
        <Route exact path="/" component={Home} />
      </Base>
    </ConnectedRouter>
  </Provider>
);
