import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import requestFindRecipe from '../../actions/recipeDetails/recipeDetails';

import RecipeDetails from './Components/RecipeDetails';
import Spinner from '../../Components/Spinner/Spinner';

import './RecipeDetailsContainer.scss';

class RecipeDetailsContainer extends Component {
  componentDidMount() {
    this.props.dispatchRequestFindRecipe(this.props.id);
  }

  render() {
    const { recipeDetails: {
      isFetching, value, didInvalidate, error,
    } } = this.props;

    if (didInvalidate) {
      return (
        <div className="recipe-details-container--center">
          <h2>{error}</h2>
        </div>
      );
    }

    if (isFetching || !Object.keys(value).length) {
      return (
        <div className="recipe-details-container--center">
          <Spinner size="large" />
        </div>
      );
    }
    return (
      <div className="recipe-details-container">
        <RecipeDetails details={value} />
      </div>
    );
  }
}

RecipeDetailsContainer.propTypes = {
  dispatchRequestFindRecipe: PropTypes.func.isRequired,
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  recipeDetails: PropTypes.shape({
    didInvalidate: PropTypes.bool,
    isFetching: PropTypes.bool,
    value: PropTypes.object,
  }).isRequired,
};

const mapStateToProps = (state, ownProps) => ({
  id: ownProps.match.params.id,
  recipeDetails: { ...state.recipeDetails },
});

const mapDispatchToProps = dispatch => bindActionCreators({
  dispatchRequestFindRecipe: requestFindRecipe,
}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RecipeDetailsContainer);
