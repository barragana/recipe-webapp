import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Search from './Search';

const colors = [
  { id: '#FFFF00', color: '#FFFF00', text: 'Yellow' },
  { id: '#000000', color: '#000000', text: 'Black' },
  { id: '#0000FF', color: '#0000FF', text: 'Blue' },
];

storiesOf('Components', module)
  .add('Search', () => (
    <Search
      list={colors}
      onChange={action('onChange - search input')}
      placeholder="Search by country name"
    />
  ));
