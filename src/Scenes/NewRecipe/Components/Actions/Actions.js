import React, { Component, Fragment } from 'react';
import PropTypes from 'prop-types';

import ActionButon from '../ActionButton/ActionButton';

import './Actions.scss';

class Actions extends Component {
  constructor(props) {
    super(props);

    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(action) {
    this.props.onClick({ id: this.props.id, action });
  }

  render() {
    const { status } = this.props;
    return (
      <div className={`actions${status !== 'done' ? ' actions--centered' : ''}`}>
        {
          (status === 'editing' && (
            <ActionButon action="done" onClick={this.handleClick} />
          )) ||
          (status === 'done' && (
            <Fragment>
              <ActionButon action="edit" onClick={this.handleClick} />
              <ActionButon action="remove" onClick={this.handleClick} />
            </Fragment>
          )) ||
          <ActionButon action="add" onClick={this.handleClick} />
        }
      </div>
    );
  }
}

Actions.propTypes = {
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  status: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

Actions.defaultProps = {
  status: 'add',
};

export default Actions;
