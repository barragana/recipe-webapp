import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import NewIngredients from './NewIngredients';

const mockData = {
  items: [],
  new: {
    id: 'new',
    status: 'new',
    value: { amount: '', name: '', unit: '' },
    errors: {},
  },
};


storiesOf('NewRecipe/Components', module)
  .add('NewIngredients', () => (
    <NewIngredients items={mockData.items} newItem={mockData.new} onActionClick={action('actions-click')} onFieldChange={action('filed-change')} />
  ));
