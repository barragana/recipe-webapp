const path = require('path');
const { scssLoader, fileLoader, babelLoader } = require('./loaders');

module.exports = {
  // the base path which will be used to resolve entry points
  context: path.join(__dirname, '../src'),

  node: {
    net: 'empty',
    tls: 'empty',
    dns: 'empty',
  },

  output: {
    // the absolute path where the compiled file will be saved
    path: path.join(__dirname, '..', 'bundle'),
    // if the webpack code-splitting feature is enabled, this is the path it'll use to download bundles
    publicPath: '/static/',
  },

  resolve: {
    // tell webpack which extensions to auto search when it resolves modules
    extensions: ['.js'],
  },

  module: {
    rules: [babelLoader, fileLoader, scssLoader],
  },
};
