const path = require('path');
const { fileLoader } = require('../webpack/loaders');

module.exports = {
  node: {
    net: 'empty',
    tls: 'empty',
    dns: 'empty'
  },
  module: {
    rules: [
      {
        test: /\.(scss|sass|css)$/i,
        loaders: [
          'style-loader',
          'css-loader',
          'sass-loader',
          {
            loader: 'postcss-loader',
            options: {
              config: {
                path: path.join(__dirname, '..', 'config', '/postcss.config.js'),
              },
            },
          },
        ],
      },
      fileLoader,
    ]
  }
}
