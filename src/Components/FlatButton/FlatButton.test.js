import React from 'react';
import { shallow } from 'enzyme';

import FlatButton from './FlatButton';

describe('Components', () => {
  describe('FlatButton', () => {
    const mockOnClick = jest.fn();

    const wrapper = shallow(
      <FlatButton onClick={mockOnClick}>
        Text
      </FlatButton>,
    );

    it(
      'render',
      () => {
        expect(wrapper.html()).not.toBeNull();
      },
    );
    it(
      'trigger mockOnClick when button is clicked',
      () => {
        const button = wrapper.find('button');
        button.simulate('click');
        expect(mockOnClick).toBeCalled();
      },
    );
  });
});
