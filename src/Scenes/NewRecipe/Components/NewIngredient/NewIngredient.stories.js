import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import NewIngredient from './NewIngredient';

const mockIngredient = {
  id: 'new',
  status: 'editing',
  value: {
    amount: '',
    name: '',
    unit: '',
  },
  errors: {
    amount: '',
    name: '',
    unit: '',
  },
};

storiesOf('NewRecipe/Components', module)
  .add('NewIngredient', () => (
    <NewIngredient onChange={action('onChange-input')} />
  ));
