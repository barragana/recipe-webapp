/* eslint-disable global-require */
const path = require('path');
const express = require('express');
const compression = require('compression');
const helmet = require('helmet');
const bodyParser = require('body-parser');
const debug = require('debug')('server');

const { errorHandling, originValidation } = require('./src/middlewares');

const PORT = process.env.PORT || 3030;
const HOST = process.env.HOST || 'localhost';

const app = express();

app.use(originValidation());
app.use(helmet());
app.use(compression());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

if (process.env.NODE_ENV !== 'production') {
  const webpack = require('webpack');
  const webpackDevMiddleware = require('webpack-dev-middleware');
  const webpackHotMiddleware = require('webpack-hot-middleware');
  const config = require('../webpack/dev.js');
  const compiler = webpack(config);

  app.use(webpackDevMiddleware(compiler, {
    publicPath: config.output.publicPath,
    noInfo: true,
    stats: 'minimal',
  }));
  app.use(webpackHotMiddleware(compiler));
} else {
  app.use('/static', express.static(path.join(__dirname, '../bundle')));
}

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, '../bundle/index.html'));
});

app.use(errorHandling);

app.listen(PORT, HOST, () => {
  debug(`Server listening on port ${PORT}!`);
});
