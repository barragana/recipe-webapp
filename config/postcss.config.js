/* eslint-disable global-require */
const scssToJson = require('scss-to-json');
const path = require('path');

const variables = scssToJson(
  path.resolve(__dirname, '..', 'src', 'styles', '_variables.scss'));

module.exports = {
  plugins: [
    /* autoprefixer for different browsers */
    require('autoprefixer'),
    /* reset inherited rules */
    require('postcss-initial')({ reset: 'inherited' }),
    /* require global variables */
    require('postcss-simple-vars')({
      variables: () => variables,
      unknown: (node, name) => {
        console.warn(`WARNING >> Unknown variable ${name}`);
      },
    }),
  ],
};
