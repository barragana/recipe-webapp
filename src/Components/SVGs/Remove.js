import React from 'react';
import PropTypes from 'prop-types';

const RemoveIcon = ({ width }) => (
  <svg width={width} height={width} xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 511.999 511.999" style={{ enableBackground: 'new 0 0 511.999 511.999' }} xmlSpace="preserve">
    <circle style={{ fill: '#E21B1B' }} cx="255.999" cy="255.999" r="255.999" />
    <g>
      <rect x="244.002" y="120.008" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -106.0397 256.0022)" style={{ fill: '#FFFFFF' }} width="24" height="271.988" />
      <rect x="120.008" y="244.007" transform="matrix(0.7071 -0.7071 0.7071 0.7071 -106.0428 256.0035)" style={{ fill: '#FFFFFF' }} width="271.988" height="24" />
    </g>
  </svg>
);

RemoveIcon.propTypes = {
  width: PropTypes.number,
};

RemoveIcon.defaultProps = {
  width: 'auto',
};

export default RemoveIcon;
