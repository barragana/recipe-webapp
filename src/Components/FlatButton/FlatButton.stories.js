import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import FlatButton from './FlatButton';

storiesOf('Components', module)
  .add('FlatButton', () => (
    <FlatButton onClick={action('input-change')}>
      FlatButton Text or Whatever
    </FlatButton>
  ));
