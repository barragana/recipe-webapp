import React from 'react';
import { storiesOf } from '@storybook/react';

import FiguresList from './FiguresList';

const mockFiguresList = [
  {
    id: 1,
    image: 'https://nutrition-assets.freeletics.com/recipes/large/mustard_deviled_eggs.jpg',
    name: 'Mustard Deviled Eggs',
  },
  {
    id: 2,
    image: 'https://nutrition-assets.freeletics.com/recipes/large/feta_scrambled_eggs.jpg',
    name: 'Feta Scrambled Eggs',
  },
  {
    id: 3,
    image: 'https://nutrition-assets.freeletics.com/recipes/large/fried_fish_with_grapefruit_salsa.jpg',
    name: 'Fried Fish With Grapefruit Salsa',
  },
  {
    id: 4,
    image: 'https://nutrition-assets.freeletics.com/recipes/large/classic_tuna_salad.jpg',
    name: 'Salade Niçoise',
  },
];

storiesOf('Home/Components', module)
  .add('FiguresList', () => (
    <div style={{ width: '100%', background: '#f8f8f8', height: '100%', overflow: 'auto' }}>
      <FiguresList figuresList={mockFiguresList} />
    </div>
  ));
