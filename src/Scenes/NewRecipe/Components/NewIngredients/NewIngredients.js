import React from 'react';
import PropTypes from 'prop-types';

import Actions from '../../Components/Actions/Actions';
import NewIngredient from '../../Components/NewIngredient/NewIngredient';

import './NewIngredients.scss';

const NewIngredients = ({ items, newItem, onActionClick, onFieldChange }) => (
  <div className="new-ingredients">
    {
      items.map((item, key) => (
        <div className="new-ingredients__item" key={`new-ingredient__item--${key}`}>
          <NewIngredient ingredient={{ id: key, ...item }} onChange={onFieldChange} disabled={item.status === 'done'} />
          <Actions id={key} status={item.status} onClick={onActionClick} />
        </div>
      ))
    }
    <div className="new-ingredients__item">
      <NewIngredient ingredient={newItem} onChange={onFieldChange} />
      <Actions id="new" status={newItem.status} onClick={onActionClick} />
    </div>
  </div>
);

NewIngredients.propTypes = {
  items: PropTypes.array.isRequired,
  newItem: PropTypes.object.isRequired,
  onActionClick: PropTypes.func.isRequired,
  onFieldChange: PropTypes.func.isRequired,
};

export default NewIngredients;
