const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const loaders = [{
  loader: 'css-loader',
  options: {
    minimize: process.env.NODE_ENV === 'production',
  },
},
'sass-loader',
{
  loader: 'postcss-loader',
  options: {
    config: {
      path: path.join(
        __dirname, '..', '..', 'config', '/postcss.config.js',
      ),
    },
  },
}];

module.exports = {
  test: /\.(scss|sass|css)$/i,
  use: process.env.NODE_ENV === 'production' ?
    ExtractTextPlugin.extract({
      fallback: 'style-loader',
      use: loaders,
    }) : [{ loader: 'style-loader' }].concat(loaders),
};
