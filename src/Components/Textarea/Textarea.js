import React from 'react';
import PropTypes from 'prop-types';

import './Textarea.scss';

const Textarea = ({ id, onChange, placeholder, value }) => (
  <textarea
    className="input"
    id={id}
    onChange={onChange}
    placeholder={placeholder}
    rows="3"
    value={value}
  />
);

Textarea.propTypes = {
  id: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string,
};

Textarea.defaultProps = {
  placeholder: '',
  value: '',
};

export default Textarea;
