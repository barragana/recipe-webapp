import React from 'react';
import PropTypes from 'prop-types';

import Seasoning from '../Seasoning/Seasoning';

import './SeasoningsList.scss';

const SeasoningsList = ({ seasoningsList }) => (
  <div className="seasonings-list">
    {
      seasoningsList.map((seasoning, key) => (
        <Seasoning key={`seasoning_${key}`} name={seasoning} />
      ))
    }
  </div>
);


SeasoningsList.propTypes = {
  seasoningsList: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default SeasoningsList;
