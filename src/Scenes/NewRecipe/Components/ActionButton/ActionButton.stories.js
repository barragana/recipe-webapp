import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import ActionButton from './ActionButton';

storiesOf('NewRecipe/Components', module)
  .add('ActionButton: Edit', () => (
    <ActionButton action="edit" onClick={action('onClick-button')} />
  ))
  .add('ActionButton: Add', () => (
    <ActionButton action="add" onClick={action('onClick-button')} />
  ))
  .add('ActionButton: Remove', () => (
    <ActionButton action="remove" onClick={action('onClick-button')} />
  ));
