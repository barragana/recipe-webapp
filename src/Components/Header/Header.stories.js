import React from 'react';
import { storiesOf } from '@storybook/react';

import Header from './Header';

storiesOf('Components', module)
  .add('Header', () => (
    <Header>
      <span>Home</span>
      <span>New Recipe</span>
    </Header>
  ));
