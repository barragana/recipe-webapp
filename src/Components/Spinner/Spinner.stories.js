import React from 'react';
import { storiesOf } from '@storybook/react';

import Spinner from './Spinner';

storiesOf('Components', module)
  .add('Spinner', () => (
    <Spinner />
  ))
  .add('Spinner: large', () => (
    <Spinner size="large" />
  ))
  .add('Spinner: small', () => (
    <Spinner size="small" />
  ));
