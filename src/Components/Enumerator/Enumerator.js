import React from 'react';
import PropTypes from 'prop-types';

import './Enumerator.scss';

const Enumerator = ({ number }) => (
  <div className="enumerator">{number}</div>
);

Enumerator.propTypes = {
  number: PropTypes.number.isRequired,
};

export default Enumerator;
