import axios from 'axios';
import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import store from '../../store';
import requestFindRecipe from './recipeDetails';
import { initialState } from '../../reducers/recipeDetails';
import {
  RECIPE_FIND_REQUEST, RECIPE_FIND_SUCCESS,
} from '../../constants/recipeDetails';
import { RECIPES_LIST_SUCCESS } from '../../constants/recipesList';

const mockRecipesList = { data:
  [
    { id: 0, name: 'Recipe 1' },
    { id: 1, name: 'Recipe 2' },
    { id: 2, name: 'Recipe 3' },
    { id: 3, name: 'Recipe 4' },
  ],
};

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

jest.mock('axios');
jest.mock('../../store');

describe('Actions', () => {
  describe('recipeDetails should', () => {
    axios.get.mockResolvedValue(mockRecipesList);

    it(
      `when recipesList has items:
          trigger the correct actions and receive the list`,
      async () => {
        store.getState.mockReturnValue({
          recipesList: { value: mockRecipesList.data },
        });

        const expectedActions = [
          { type: RECIPE_FIND_REQUEST },
          { type: RECIPE_FIND_SUCCESS, recipeDetails: mockRecipesList.data[2] },
        ];

        const storeMocked = mockStore({
          recipesList: { ...initialState, value: mockRecipesList.data },
        });

        await storeMocked.dispatch(requestFindRecipe(2));
        expect(storeMocked.getActions()).toEqual(expectedActions);
      },
    );

    it(
      `when recipesList is empty:
          trigger the correct actions and receive the list`,
      async () => {
        store.getState.mockReturnValue({
          recipesList: { value: [] },
        });

        const expectedActions = [
          { type: RECIPE_FIND_REQUEST },
          { type: RECIPES_LIST_SUCCESS, recipesList: mockRecipesList.data },
          { type: RECIPE_FIND_SUCCESS, recipeDetails: mockRecipesList.data[2] },
        ];

        const storeMocked = mockStore({
          recipesList: { ...initialState, value: [] },
        });

        await storeMocked.dispatch(requestFindRecipe(2));
        expect(storeMocked.getActions()).toEqual(expectedActions);
      });
  });
});
