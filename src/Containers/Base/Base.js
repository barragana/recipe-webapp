import React from 'react';
import PropTypes from 'prop-types';

import Header from '../../Components/Header/Header';
import Link from '../../Components/Link/Link';

import './Base.scss';

const Base = ({ children }) => (
  <div className="base">
    <div className="base__header">
      <Header>
        <div className="base__wrapper">
          <div className="base__logo">
            <span className="base__link">
              <Link to="/">
                Recipe Amazing World
              </Link>
            </span>
          </div>
          <div className="base__menu">
            <span className="base__link">
              <Link to="/recipes/new">
                New Recipe
              </Link>
            </span>
          </div>
        </div>
      </Header>
    </div>
    {children}
  </div>
);

Base.propTypes = {
  children: PropTypes.node.isRequired,
};

export default Base;
