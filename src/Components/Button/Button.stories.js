import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';

import Button from './Button';

storiesOf('Components', module)
  .add('Button', () => (
    <Button onClick={action('input-change')}>
      Button Text or Whatever
    </Button>
  ));
