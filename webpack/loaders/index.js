/* eslint-disable global-require */
module.exports = {
  babelLoader: require('./babelLoader'),
  fileLoader: require('./fileLoader'),
  scssLoader: require('./scssLoader'),
};
