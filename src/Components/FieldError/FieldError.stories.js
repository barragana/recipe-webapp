import React from 'react';
import { storiesOf } from '@storybook/react';

import FieldError from './FieldError';

storiesOf('Components', module)
  .add('FieldError', () => (
    <FieldError>
      FlatButton Text or Whatever
    </FieldError>
  ));
