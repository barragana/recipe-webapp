import React from 'react';
import PropTypes from 'prop-types';

import './FlatButton.scss';

const FlatButton = ({ children, onClick }) => (
  <button className="flat-button" onClick={onClick}>
    {children}
  </button>
);

FlatButton.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired,
};

export default FlatButton;
