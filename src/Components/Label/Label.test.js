import React from 'react';
import { shallow } from 'enzyme';

import Label from './Label';

describe('Components', () => {
  describe('Label', () => {
    describe('should', () => {
      const mockOnChange = jest.fn();

      const wrapper = shallow(
        <Label htmlFor="name" onChange={mockOnChange}>
          Input label
        </Label>,
      );

      it(
        'render',
        () => {
          expect(wrapper.html()).not.toBeNull();
        },
      );

      it(
        'render label text',
        () => {
          expect(wrapper.contains('Input label')).toEqual(true);
        },
      );
    });
  });
});
